import React, { useState, useMemo } from 'react';
import Card from '../UI/Card';
import DataFilter from './DataFilter';
import './DataList.css';
import Pagination from '../Pagination/Pagination';

const DataList = (props) => {
  const [filteredYear, setFilteredYear] = useState('2023');
  const [searchQuery, setSearchQuery] = useState('');
  const [rowCount, setRowCount] = useState(20);
  const [dateFilter, setDateFilter] = useState('');
  const [sourceFilter, setSourceFilter] = useState('');
  const [currentPage, setCurrentPage] = useState(1);

  const { items } = props || {};

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  const searchChangeHandler = (event) => {
    setSearchQuery(event.target.value);
  };

  const searchHandler = () => {
    // handle search logic here
  };

  const rowChangeHandler = (event) => {
    setRowCount(parseInt(event.target.value));
  };

  const dateFilterChangeHandler = (value) => {
    setDateFilter(value);
  };

  const sourceFilterChangeHandler = (value) => {
    setSourceFilter(value);
  };

  const filteredData = props.items.filter((item) => {
    const time = new Date(item.date);
    return (
      (!dateFilter || time.toISOString().startsWith(dateFilter)) &&
      time.getFullYear().toString() === filteredYear &&
      (!sourceFilter || item.source === sourceFilter) &&
      item.country.toLowerCase().includes(searchQuery.toLowerCase())
    );
  });

  const totalNumPages = Math.ceil(filteredData.length / rowCount);

  const generatePageNumbers = () => {
    const pageNumbers = [];
    for (let i = 1; i <= totalNumPages; i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  };

  const mediumTime = new Intl.DateTimeFormat('en', {
    timeStyle: 'medium',
    dateStyle: 'short',
  });

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected + 1);
  };

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * rowCount;
    const lastPageIndex = firstPageIndex + rowCount;
    return filteredData.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, filteredData, rowCount]);

  return (
    <div>
      <Card className="expenses">
        <DataFilter
          selected={filteredYear}
          dateFilter={dateFilter}
          searchFilter={searchQuery}
          sourceFilter={sourceFilter}
          onChangeFilter={filterChangeHandler}
          onDateFilterChange={dateFilterChangeHandler}
          onSearchFilterChange={searchChangeHandler}
          onSourceFilterChange={sourceFilterChangeHandler}
        />
        <div className="rowCount">
          <label htmlFor="rowCount">Rows per page : </label>
          <select id="rowCount" value={rowCount} onChange={rowChangeHandler}>
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="4000">4000</option>
          </select>
        </div>
        <table>
          <thead>
            <tr>
              <th>Country</th>
              <th>Source</th>
              <th>Total Confirmed</th>
              <th>Total Recovered</th>
              <th>Total Death</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {currentTableData.map((item, index) => {
              const time = new Date(item.date);
              return (
                <tr
                  key={index}
                  style={{
                    backgroundColor: index % 2 === 0 ? '#e2e2e2' : 'white',
                  }}
                >
                  <td>{item.country}</td>
                  <td>{item.source}</td>
                  <td>{item.confirmed}</td>
                  <td>{item.recovered}</td>
                  <td>{item.deaths}</td>
                  <td>{mediumTime.format(time)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Pagination
          className="pagination-bar"
          currentPage={2}
          totalCount={totalNumPages}
          pageSize={rowCount}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </Card>
    </div>
  );
};

export default DataList;
