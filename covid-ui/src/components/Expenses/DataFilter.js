import React from 'react';

import './DataFilter.css';

const DataFilter = (props) => {
  const dropdownChangeHandler = (event) => {
    props.onChangeFilter(event.target.value);
  };

  const dateChangeHandler = (event) => {
    props.onDateFilterChange(event.target.value);
  };

  const sourceChangeHandler = (event) => {
    props.onSourceFilterChange(event.target.value);
  };

  return (
    <div className="search-filter">
      <div className="search-filter__control">
        <div className="search__control">
          <input
            type="date"
            id="date"
            value={props.dateFilter}
            min="2019-01-01"
            max="2023-12-31"
            onChange={dateChangeHandler}
          />
        </div>
        <select value={props.selected} onChange={dropdownChangeHandler}>
          <option value="2023">2023</option>
          <option value="2022">2022</option>
        </select>
        <select value={props.sourceFilter} onChange={sourceChangeHandler}>
          <option value="">All Sources</option>
          <option value="disease.sh">disease.sh</option>
          <option value="covid19api">covid19api</option>
          <option value="JHU">JHU</option>
        </select>
        <div className="search">
          <input
            type="text"
            id="search"
            placeholder="Country name"
            value={props.searchFilter}
            onChange={props.onSearchFilterChange}
          />
        </div>
      </div>
    </div>
  );
};

export default DataFilter;
