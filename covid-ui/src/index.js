import React from 'react';
import ReactDOM from 'react-dom/client';
import DataList from './components/Expenses/DataList';

import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// const container = ReactDOM.createRoot(document.getElementById('container'));

root.render(<App />);
// container.render(<DataList />);

// ReactDOM.render(
//   <DataList itemsPerPage={4} />,
//   document.getElementById('container')
// );
