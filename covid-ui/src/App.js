import React, { useState, useEffect } from 'react';

import Header from './components/Header/Header';
import DataList from './components/Expenses/DataList';

const UsingFetch = () => {
  const [table, setTable] = useState([]);

  const fetchData = () => {
    fetch('http://127.0.0.1:5000/cases/all')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setTable(data);
      });
  };

  const item = [
    {
      confirmed: 211623,
      country: 'Afghanistan',
      date: '2023-04-10 14:57:20',
      deaths: 7896,
      recovered: 192468,
      source: 'disease.sh',
    },
    {
      confirmed: 334701,
      country: 'Albania',
      date: '2023-04-10 14:57:20',
      deaths: 3602,
      recovered: 329407,
      source: 'disease.sh',
    },
    {
      confirmed: 271613,
      country: 'Algeria',
      date: '2023-04-10 14:57:20',
      deaths: 6881,
      recovered: 182894,
      source: 'disease.sh',
    },
    {
      confirmed: 47930,
      country: 'Andorra',
      date: '2023-04-10 14:57:20',
      deaths: 165,
      recovered: 47563,
      source: 'disease.sh',
    },
  ];

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <Header />
      <DataList items={table} />
    </div>
  );
};

export default UsingFetch;
