
def search_country(es, index, country_name, size):
    # define the search query
    query = {
        "query": {
            "match": {
                "country": country_name
            }
        },
        "size": size}

    # execute the search query
    results = es.search(index=index, body=query)

    # iterate over the results and print each document
    return [hit['_source'] for hit in results['hits']['hits']]


def search_date(es, index, date, size):
    # define the search query
    query = {
        "query": {
            "match": {
                "date": date
            }
        },
        "size": size}

    # execute the search query
    results = es.search(index=index, body=query)

    # iterate over the results and print each document
    return [hit['_source'] for hit in results['hits']['hits']]


def search_source(es, index, source, size):
    # define the search query
    query = {
        "query": {
            "match": {
                "source": source
            }
        },
        "size": size}

    # execute the search query
    results = es.search(index=index, body=query)

    # iterate over the results and print each document
    return [hit['_source'] for hit in results['hits']['hits']]


def search_all(es, index, size):
    # define the search query
    query = {
        "query": {
            "match_all": {}
        },
        "size": size
}

    # execute the search query
    results = es.search(index=index, body=query)

    # iterate over the results and print each document
    return [hit['_source'] for hit in results['hits']['hits']]