from elasticsearch import Elasticsearch
from es_query import search_country, search_date, search_all
from flask import Flask, jsonify
from flask_cors import CORS
import json

with open('../mini-project.config', 'r') as f:
    config = json.load(f)

# ElasticSearch configuration
ES_SERVER = config['elasticsearch']['server']
ES_INDEX = config['elasticsearch']['index']

es = Elasticsearch(ES_SERVER)

app = Flask(__name__)
CORS(app)


@app.route('/cases/<country>')
def get_cases_by_country(country):
    data = search_country(es, ES_INDEX, country, 10000)
    return jsonify(data)


@app.route('/cases/all')
def get_all_cases():
    data = search_all(es, ES_INDEX, 10000)
    return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True)
