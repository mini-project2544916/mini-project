import requests
from datetime import datetime
import json

date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def pull_covid19_api():
    url = "https://api.covid19api.com/summary"
    response = requests.get(url)
    data = response.json()
    formatted_data = []

    for item in data['Countries']:
        country = item['Country']
        source = 'covid19api'
        confirmed = item['TotalConfirmed']
        deaths = item['TotalDeaths']
        recovered = item['TotalRecovered']
        formatted_data.append(
            {'country': country, 'source': source, 'confirmed': confirmed, 'deaths': deaths, 'recovered': recovered,
             'date': datetime.strptime(item['Date'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%Y-%m-%d %H:%M:%S')})

    return data, formatted_data


def pull_covid19_disease():
    url = "https://disease.sh/v3/covid-19/countries"
    response = requests.get(url)
    data = response.json()
    formatted_data = []

    for item in data:
        country = item['country']
        source = 'disease.sh'
        confirmed = item['cases']
        deaths = item['deaths']
        recovered = item['recovered']
        formatted_data.append(
            {'country': country, 'source': source, 'confirmed': confirmed, 'deaths': deaths, 'recovered': recovered,
             'date': date})

    return data, formatted_data


def pull_covid19_jhu():
    url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/03-09-2023.csv"
    response = requests.get(url)
    lines = response.text.strip().split('\n')
    headers = lines[0].split(',')
    data = []
    formatted_data = []

    for line in lines[1:]:
        row = line.split(',')
        item = {}
        for i in range(len(headers)):
            item[headers[i]] = row[i]
        data.append(item)

    for item in data:
        country = item['Country_Region'] + " " + item['Province_State']
        source = 'JHU'
        confirmed = item['Confirmed']
        deaths = item['Deaths']
        recovered = item['Recovered']
        formatted_data.append(
            {'country': country, 'source': source, 'confirmed': confirmed, 'deaths': deaths, 'recovered': recovered,
             'date': item['Last_Update']})

    return data, formatted_data

