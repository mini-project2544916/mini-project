from pull_stream import pull_covid19_disease, pull_covid19_api, pull_covid19_jhu
from push_stream import push_data
from store_stream import store_data
import json

# Load configuration from JSON file
with open('../mini-project.config', 'r') as f:
    config = json.load(f)

# Pull raw data from various sources
data_1, formatted_data1 = pull_covid19_disease()
data_2, formatted_data2 = pull_covid19_api()
data_3, formatted_data3 = pull_covid19_jhu()

formatted_data = []
formatted_data.extend(formatted_data1 + formatted_data2 + formatted_data3)

with open('../output.json', 'w') as outfile:
    json.dump(formatted_data, outfile, indent=4)

# Push data to Kafka
topic = config['kafka']['topic']
bootstrap_servers = config['kafka']['server']
push_data(formatted_data, topic, bootstrap_servers)

# Store raw data in MongoDB
database = config['mongodb']['database']
collection = config['mongodb']['collection']
host = config['mongodb']['server']
store_data(data_1, database, collection, host)
store_data(data_2['Countries'], database, collection, host)
store_data(data_3, database, collection, host)
