from pymongo import MongoClient

def store_data(data, database, collection, host):
    client = MongoClient(host)
    db = client[database]
    collection = db[collection]
    collection.insert_many(data)