import json
from kafka import KafkaProducer

def push_data(data, topic, bootstrap_servers):
    producer = KafkaProducer(bootstrap_servers=bootstrap_servers)
    for d in data:
        message = json.dumps(d).encode('utf-8')
        producer.send(topic, value=message)
