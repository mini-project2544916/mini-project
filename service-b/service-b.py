from kafka import KafkaConsumer
from elasticsearch import Elasticsearch
import json

with open('../mini-project.config', 'r') as f:
    config = json.load(f)

# Kafka configuration
topic = config['kafka']['topic']
bootstrap_servers = config['kafka']['server']

# ElasticSearch configuration
ES_SERVER = config['elasticsearch']['server']
ES_INDEX = config['elasticsearch']['index']

# Create Kafka consumer
consumer = KafkaConsumer(topic, bootstrap_servers=bootstrap_servers)

# Create ElasticSearch client
es = Elasticsearch(ES_SERVER)

# create a list to hold the data to be pushed to Elasticsearch
bulk_data = []
# iterate over the messages received from Kafka
for message in consumer:
    # decode the message and load it as JSON
    data = json.loads(message.value.decode('utf-8'))
    # create an index operation for Elasticsearch
    index_op = {
        "index": {
            "_index": "covid-data",
            "_type": "doc"
        }
    }

    # add the data to the bulk data list
    bulk_data.append(index_op)
    bulk_data.append(data)

    # if the bulk data list has reached a certain size, push it to Elasticsearch
    if len(bulk_data) >= 2000:
        # use the bulk API to push the data to Elasticsearch
        es.bulk(index=ES_INDEX, body=bulk_data, request_timeout=30)

        # clear the bulk data list
        bulk_data = []

# push any remaining data to Elasticsearch
if len(bulk_data) > 0:
    # use the bulk API to push the data to Elasticsearch
    es.bulk(index=ES_INDEX, body=bulk_data, request_timeout=30)

